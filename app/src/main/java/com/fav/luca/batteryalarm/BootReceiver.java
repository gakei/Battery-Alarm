package com.fav.luca.batteryalarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v("BootReceiver", intent.getAction());

        context.startService(new Intent(context, BatteryService.class));
    }
}
