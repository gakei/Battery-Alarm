package com.fav.luca.batteryalarm;

import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

public class BatteryService extends Service {

    private static final String MIN_VALUE = "min";
    private static final String MAX_VALUE = "max";
    private static final String FLAG = "FLAG";
    private static final String VAL = "VAL";
    private static final int SYSNOTIFVALUE = 15;

    private short level, min, max, charging, flag;
    private boolean prevmin = false, prevmax = false;

    private BroadcastReceiver inputRec = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            flag = (short) intent.getIntExtra(FLAG, -1);
            if (flag == 1) {
                min = (short) intent.getIntExtra(VAL, -1);
                prevmin = false;
                //Log.d("localreceiver", "got min" + String.valueOf(min));
                notifManager.cancelAll();
            } else if (flag == 2) {
                max = (short) intent.getIntExtra(VAL, -1);
                prevmax = false;
                //Log.d("localreceiver", "got max" + String.valueOf(max));
                notifManager.cancelAll();
            }
        }
    };
    private NotificationCompat.Builder notifBuilder;
    private NotificationManager notifManager;
    private long[] arr, mut;
    private BroadcastReceiver batInfoRec = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Non so se legge tutte le volte entrambi o solo quello che si aggiorna
            charging = (short) intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            level = (short) intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            //Log.d("receiver", "got charg" + String.valueOf(charging));
            //Log.d("receiver", "got level" + String.valueOf(level));

            if (charging == 0) {
                if (prevmax) {
                    prevmax = false;
                    notifManager.cancelAll();
                }
                if (level < min && level > SYSNOTIFVALUE && !prevmin) {
                    sendNotification(1,true);
                    prevmin = true;
                } else if (level < min && level > SYSNOTIFVALUE) {
                    sendNotification(1,false);
                } else if (level <= SYSNOTIFVALUE) {
                    notifManager.cancelAll();
                }
            } else {
                if (prevmin) {
                    prevmin = false;
                    notifManager.cancelAll();
                }
                if (level > max && !prevmax) {
                    sendNotification(2,true);
                    prevmax = true;
                } else if (level > max) {
                    sendNotification(2,false);
                }
            }


        }
    };
    public BatteryService() {
    }

    private void sendNotification(int minmax, boolean newnotif) {

        notifBuilder.setSmallIcon(R.drawable.notificon)
                .setOnlyAlertOnce(true)
                .setVibrate(arr)
                .setContentText(getResources().getString(R.string.text_notif) + Integer.toString(level));
        if (newnotif) {
            notifBuilder.setVibrate(arr);
        } else {
            notifBuilder.setVibrate(mut);
        }
        if (minmax == 1) {
            notifBuilder.setContentTitle(getResources().getString(R.string.title_min));
        } else if (minmax == 2) {
            notifBuilder.setContentTitle(getResources().getString(R.string.title_max));
        }
        notifManager.notify(9999, notifBuilder.build());
        //Log.d("mandata notifica","9999");
    }

    private void getMinMaxprefs() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Battery", Context.MODE_PRIVATE);
        min = (short) prefs.getInt(MIN_VALUE, 101);
        max = (short) prefs.getInt(MAX_VALUE, -1);
        if (min==101 || max==-1)
            Toast.makeText(this, R.string.no_prefs, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCreate() {
        getMinMaxprefs();
        //Toast.makeText(this, "Service created",Toast.LENGTH_LONG).show();
        notifBuilder = new NotificationCompat.Builder(getApplicationContext());
        arr = new long[]{100, 100, 100, 100, 100, 100};
        mut = new long[]{};
        notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        registerReceiver(batInfoRec, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(inputRec, new IntentFilter("com.luca.fav.inputs"));
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // do something useful
        //Toast.makeText(this, "Service onStartCommand",Toast.LENGTH_LONG).show();
        return Service.START_STICKY;

    }

    @Override
    public void onDestroy() {
        //Toast.makeText(this, "Service destroyed",Toast.LENGTH_LONG).show();
        unregisterReceiver(batInfoRec);
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(inputRec);
        notifManager.cancelAll();
        notifManager = null;
    }

    @Override
    public IBinder onBind(Intent intent) {
        //  Return the communication channel to the service.
        return null;
    }

}
