package com.fav.luca.batteryalarm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {
    private static final String MIN_VALUE = "min";
    private static final String MAX_VALUE = "max";
    private static final String FLAG = "FLAG";
    private static final String VAL = "VAL";

    private EditText minEdit, maxEdit;
    private int min, max;


    private void updateMinMax(int a) {
        Intent i = new Intent("com.luca.fav.inputs");
        i.putExtra(FLAG, a);
        i.putExtra(VAL, (a==1)?min:max);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);

    }

    private void getMinMaxprefs() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Battery", Context.MODE_PRIVATE);
        // Se non esiste pref dai valore arbitrario
        int check=0;
        min = prefs.getInt(MIN_VALUE, 101);
        if (min==101) {
            min = 50;
            check=1;
        }
        max = prefs.getInt(MAX_VALUE, -1);
        if (max==-1) {
            max=70;
            check=1;
        }
        if (check==1) {
            saveMinMax();
        }

        minEdit.setText(Integer.toString(min));
        maxEdit.setText(Integer.toString(max));
    }

    private void saveMinMax() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences("Battery", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEdit = prefs.edit();
        prefsEdit.putInt(MIN_VALUE, min);
        prefsEdit.putInt(MAX_VALUE, max);
        prefsEdit.apply();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        minEdit = (EditText) findViewById(R.id.minIn);
        maxEdit = (EditText) findViewById(R.id.maxIn);

        getMinMaxprefs();

        minEdit.setOnKeyListener(new enterListener());
        maxEdit.setOnKeyListener(new enterListener());

        startService(new Intent(getApplicationContext(), BatteryService.class));
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveMinMax();
        saveMinMax();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveMinMax();
        saveMinMax();
    }

    private class enterListener implements EditText.OnKeyListener {

        @Override
        public boolean onKey(View view, int i, KeyEvent event) {
            if (event.getAction() == KeyEvent.ACTION_DOWN && i == KeyEvent.KEYCODE_ENTER) {
                if (view == minEdit) {
                    try {
                        min = Integer.parseInt(minEdit.getText().toString());
                        updateMinMax(1);
                    } catch (NumberFormatException e) {
                        min = 0;
                    }
                } else if (view == maxEdit) {
                    try {
                        max = Integer.parseInt(maxEdit.getText().toString());
                        updateMinMax(2);
                    } catch (NumberFormatException e) {
                        max = 0;
                    }
                }
            }
            return false;
        }
    }

}
