Battery Alarm
=============

Simple application that send notification when the phone charge is out of a defined range (default 50%-70%). 
It runs an activity to set values and a service that receive battery change broadcast and send notifications.
The communication between the activity and the service uses LocalBroadcastManager.
The application is launched on boot (BootReceiver.class) and the service is run sticky.
You can set only two values:
1. Min = A notification is sent if the battery level is below this value and the phone is not charging;
2. Max = A notification is sent when the battery level is above this value and the phone is charging.